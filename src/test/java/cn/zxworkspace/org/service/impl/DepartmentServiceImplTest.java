package cn.zxworkspace.org.service.impl;

import cn.zxworkspace.Basic;
import cn.zxworkspace.org.domain.Department;
import cn.zxworkspace.org.query.DataTypeQuery;
import cn.zxworkspace.org.query.DepartmentQuery;
import cn.zxworkspace.org.service.IDataTypeService;
import cn.zxworkspace.org.service.IDepartmentService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class DepartmentServiceImplTest extends Basic {
    @Autowired
    private IDepartmentService departmentService;

    @Autowired
    private IDataTypeService dataTypeService;

    @Test
    public void add() {
        Department department = new Department();
        department.setName("xixixi");
        departmentService.add(department);
    }

    @Test
    public void delete() {
        departmentService.delete(46L);
    }

    @Test
    public void update() {
        Department department = new Department();
        department.setId(48L);
        department.setName("xixixi");
        departmentService.update(department);
    }

    @Test
    public void queryById() {
        System.out.println(departmentService.queryById(48L));
    }

    @Test
    public void queryAll() {
        departmentService.queryAll().forEach(System.out::println);
    }

    @Test
    public void queryPage() {
        DepartmentQuery query = new DepartmentQuery();
        departmentService.queryPage(query).getRows().forEach(System.out::println);
    }
    @Test
    public void test1(){
        dataTypeService.queryAll().forEach(System.out::println);
        DataTypeQuery query = new DataTypeQuery();
        dataTypeService.queryPage(query).getRows().forEach(System.out::println);
    }
}