package cn.zxworkspace.org.mapper;

import cn.zxworkspace.Basic;
import cn.zxworkspace.org.domain.Department;
import cn.zxworkspace.org.query.DepartmentQuery;
import cn.zxworkspace.org.service.IDepartmentService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class DepartmentMapperTest extends Basic {

    @Autowired
    private IDepartmentService departmentService;
    @Test
    public void save() {
        Department department = new Department();
        department.setName("xxxxx");
        departmentService.add(department);
    }

    @Test
    public void remove() {
        departmentService.delete(47L);
    }

    @Test
    public void update() {
        Department department = new Department();
        department.setId(47L);
        department.setName("nnn");
        departmentService.update(department);
    }

    @Test
    public void loadById() {
        System.out.println(departmentService.queryById(1L));
    }

    @Test
    public void loadAll() {
        System.out.println(departmentService.queryAll());
    }

    @Test
    public void queryCount() {
        DepartmentQuery departmentQuery = new DepartmentQuery();
        departmentService.queryPage(departmentQuery).getRows().forEach(System.out::println);
    }

    @Test
    public void queryData() {
    }
}