package cn.zxworkspace.customer.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.customer.domain.UserAddress;

public interface UserAddressMapper extends BaseMapper<UserAddress> {
}
