package cn.zxworkspace.customer.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.org.domain.Shop;

public interface UserMapper extends BaseMapper<User> {

    User findByPhone(String phone);

    User loadByLogininfoId(Long id);
}
