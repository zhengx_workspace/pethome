package cn.zxworkspace.customer.mapper;


import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.customer.domain.WxUser;

public interface WxUserMapper extends BaseMapper<WxUser> {
    WxUser loadByOpenId(String openid);

    WxUser findById(Object openid);
}
