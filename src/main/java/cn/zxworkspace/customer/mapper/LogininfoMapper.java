package cn.zxworkspace.customer.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.customer.domain.User;

public interface LogininfoMapper extends BaseMapper<Logininfo> {

    Logininfo findByUsername(String username);

    Logininfo loadByUserId(Long user_id);
}
