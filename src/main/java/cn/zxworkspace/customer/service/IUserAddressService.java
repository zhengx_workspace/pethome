package cn.zxworkspace.customer.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.customer.domain.UserAddress;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface IUserAddressService extends IBaseService<UserAddress> {
}
