package cn.zxworkspace.customer.service.impl;

import cn.zxworkspace.basic.constant.SmsConstant;
import cn.zxworkspace.basic.exception.BasicException;
import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.HttpClientUtils;
import cn.zxworkspace.basic.util.MD5Utils;
import cn.zxworkspace.basic.util.StrUtils;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.customer.domain.WxUser;
import cn.zxworkspace.customer.dto.LoginDto;
import cn.zxworkspace.customer.mapper.LogininfoMapper;
import cn.zxworkspace.customer.mapper.UserMapper;
import cn.zxworkspace.customer.mapper.WxUserMapper;
import cn.zxworkspace.customer.service.ILoginService;
import cn.zxworkspace.customer.service.ILogininfoService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
@Service
public class LoginServiceImpl implements ILoginService {
    @Autowired
    private LogininfoMapper logininfoMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private WxUserMapper wxUserMapper;
    @Autowired
    private UserMapper userMapper;
    @Override
    public Map<String, Object> accountLogin(LoginDto loginDto) {
        //校验类型
        if(loginDto.getType()==null){
            throw new BasicException("我不知道你是前端还是后端，请传一个type来！");
        }
        //去数据库查询用户名是否存在
        Logininfo logininfo = logininfoMapper.findByUsername(loginDto.getUsername());
        if(logininfo==null){
            throw new BasicException("用户名不存在，请重新输入");
        }
        //然后对比密码是否正确（加密以后的判断）
        //数据库中的密码（盐值加密）
        String passwordTmp = logininfo.getPassword();
        //获取数据库中同样账号的盐值。来对密码进行对比
        String salt = logininfo.getSalt();
        //如果数据库中的密码和传入对象的密码加盐值能对比成功，则密码正确
        String md5Pwd = MD5Utils.encrypByMd5(loginDto.getPassword() + salt);
        if(!passwordTmp.equals(md5Pwd)){
            throw new BasicException("密码不正确！！");
        }
        //登录成功之后村redis
        String token = UUID.randomUUID().toString();
        //设置保存redis时间为30分钟
        redisTemplate.opsForValue().set(
                token,
                logininfo,
                30,
                TimeUnit.MINUTES
        );
        //返回登录的必要信息
        Map<String,Object> map = new HashMap<>();
        map.put("token", token);
        map.put("logininfo",logininfo);
        return map;
    }

    /**
     * 微信扫码登录
     * @param code
     */
    @Override
    public AjaxResult wechatLogin(String code) {
        if(StringUtils.isEmpty(code)){
            throw new BasicException("参数不能为空");
        }
        //根据code从微信服务端中获取token和openid
        String tokenUrl = SmsConstant.GET_TOKEN_URL
                .replace("APPID", SmsConstant.APPID)
                .replace("SECRET", SmsConstant.SECRET)
                .replace("CODE", code);
        String urlTmp = HttpClientUtils.httpGet(tokenUrl);
        System.out.println(urlTmp);
        //转为一个对象
        JSONObject jsonObject = JSONObject.parseObject(urlTmp);
        System.out.println(jsonObject);
        //获取对象中的两个属性。
        String access_token = jsonObject.getString("access_token");
        String openid = jsonObject.getString("openid");
        //查询数据库中是否有这个对象
        WxUser wxUser = wxUserMapper.loadByOpenId(openid);
        if(wxUser!=null && wxUser.getUser_id()!=null){
            //说明曾今已经扫码了,直接免密登录
            Logininfo logininfo = logininfoMapper.loadByUserId(wxUser.getUser_id());
            String token = UUID.randomUUID().toString();//一定要加toString();
            //3.1 存redis
            redisTemplate.opsForValue().set(
                    token,
                    logininfo,//对象需要序列化
                    30,
                    TimeUnit.MINUTES
            );
            //3.2 返回登录成功的必要信息
            Map<String,Object> map = new HashMap<>();
            map.put("token", token);
            //我们作为高级技师,应该需要考虑到,将一些敏感信息屏蔽掉
            //logininfo.setSalt(null);
            //logininfo.setPassword(null);
            map.put("logininfo", logininfo);
            System.out.println(map);
            return AjaxResult.me().setResultObj(map);
        } else {
            String binderParams = "?accessToken="+access_token+"&openId="+openid;
            System.out.println(binderParams);
            return AjaxResult.me().setSuccess(false).setResultObj(binderParams);
        }

    }

    /**
     * 绑定微信
     * @param params
     * @return
     */
    @Override
    public Map<String, Object> binderWechat(Map<String, String> params) {
        String phone = params.get("phone");
        String verifyCode = params.get("verifyCode");
        String accessToken = params.get("accessToken");
        String openId = params.get("openId");
        //参数校验
        if(StringUtils.isEmpty(phone) || StringUtils.isEmpty(verifyCode)
                || StringUtils.isEmpty(accessToken) || StringUtils.isEmpty(openId)){
            throw new BasicException("参数不能为空");
        }
        //判断验证码是否失效
        String smsCode = SmsConstant.BUSINESS_BINDER_PREFIX+phone;
        Object userRegister = redisTemplate.opsForValue().get(smsCode);
        if(userRegister==null){
            throw new BasicException("验证码已失效，请重新获取！");
        }
        //获取被拼接了的验证码
        String code = ((String)userRegister).split(":")[0];
        if(!code.equals(verifyCode)){
            throw new BasicException("验证码错误！");
        }
        //查询手机号是否被注册过 t_user
        User user = userMapper.findByPhone(phone);
        Logininfo logininfo =null;
        if(user!=null){
            //如果存在。就需要把这个logininfo对象查出来做绑定
            logininfo = logininfoMapper.loadById(user.getLogininfo_id());
        } else {
            //根据手机号生成一个user对象
            user = initUser(phone);
            //再根据这个新对象生成新的logininfo对象
            logininfo = user2Logininfo(user);
            logininfoMapper.save(logininfo);
            //先保存logininfo对象返回自增id。再保存user
            user.setLogininfo_id(logininfo.getId());
            userMapper.save(user);
        }
        // 调用微信，查询用户微信基本信息
        String getWxUserURL = SmsConstant.GET_WXUSER_URL
                .replace("ACCESS_TOKEN", accessToken)
                .replace("OPENID", openId);
        String wxUserStr = HttpClientUtils.httpGet(getWxUserURL);
        WxUser wxUser = JSONObject.parseObject(wxUserStr, WxUser.class);
        System.out.println(user);
        wxUser.setUser_id(user.getId());//绑定用户
        wxUserMapper.save(wxUser);
        //直接免密登录
        String token = UUID.randomUUID().toString();//一定要加toString();
        //3.1 存redis
        redisTemplate.opsForValue().set(
                token,
                logininfo,//对象需要序列化
                30,
                TimeUnit.MINUTES
        );
        //3.2 返回登录成功的必要信息
        Map<String,Object> map = new HashMap<>();
        map.put("token", token);
        //我们作为高级技师,应该需要考虑到,将一些敏感信息屏蔽掉
        logininfo.setSalt(null);
        logininfo.setPassword(null);
        map.put("logininfo", logininfo);
        return map;
    }
    private User initUser(String phone) {
        User user = new User();
        user.setUsername(phone);
        user.setPhone(phone);
        String salt = StrUtils.getComplexRandomString(32);
        //使用盐值加密
        String md5Pwd = MD5Utils.encrypByMd5("1" + salt);
        //应该发个短信告诉客人，您的初始密码是，请及时修改
        user.setSalt(salt);
        user.setPassword(md5Pwd);
        return user;
    }

    private Logininfo user2Logininfo(User user) {
        Logininfo logininfo = new Logininfo();
        // 同名原则拷贝
        BeanUtils.copyProperties(user, logininfo);
        logininfo.setType(1);
        return logininfo;
    }
}
