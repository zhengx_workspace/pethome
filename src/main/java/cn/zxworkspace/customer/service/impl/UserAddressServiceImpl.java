package cn.zxworkspace.customer.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.customer.domain.UserAddress;
import cn.zxworkspace.customer.service.IUserAddressService;
import org.springframework.stereotype.Service;



@Service
public class UserAddressServiceImpl extends BaseServiceImpl<UserAddress> implements IUserAddressService {

}
