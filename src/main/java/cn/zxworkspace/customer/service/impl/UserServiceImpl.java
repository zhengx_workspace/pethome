package cn.zxworkspace.customer.service.impl;

import cn.zxworkspace.basic.constant.SmsConstant;
import cn.zxworkspace.basic.exception.BasicException;
import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.basic.util.MD5Utils;
import cn.zxworkspace.basic.util.StrUtils;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.customer.dto.UserDto;
import cn.zxworkspace.customer.mapper.LogininfoMapper;
import cn.zxworkspace.customer.mapper.UserMapper;
import cn.zxworkspace.customer.service.IUserService;

import cn.zxworkspace.org.mapper.EmployeeMapper;

import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.SocketUtils;
import org.springframework.util.StringUtils;

@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements IUserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private LogininfoMapper logininfoMapper;


    @Override
    public void registerByPhone(UserDto dto) {
        if(StringUtils.isEmpty(dto.getPhone())||
                StringUtils.isEmpty(dto.getPassword())||
                StringUtils.isEmpty(dto.getConfirmPassword())||
                StringUtils.isEmpty(dto.getRegisterCode())
        ){
            throw new BasicException("参数有误，请好好输入！");
        }
        // 2.两次密码是否一致
        if(!dto.getPassword().equals(dto.getConfirmPassword())){
            throw new BasicException("密码不一致！！");
        }
        // 3.用户不能被注册
        User userTmp = userMapper.findByPhone(dto.getPhone());
        if(userTmp!=null){
            throw new BasicException("用户已注册！");
        }
        //二:校验验证码
        Object codeTmp = redisTemplate.opsForValue().get(SmsConstant.USER_REGISTER_CONSTANT + dto.getPhone());
        // 1.验证码是否过期
        if(codeTmp==null){
            throw new BasicException("验证码已过期，请重新获取");
        }
        // 2.验证码是否正确
        String code = ((String)codeTmp).split(":")[0];
        if(!dto.getRegisterCode().equalsIgnoreCase(code)){
            throw new BasicException("验证码错误。");
        }
        //三:存数据
        User user =userDto2User(dto);
        Logininfo logininfo = user2Logininfo(user);
        // 1.保存logininfo（返回自增id）
        logininfoMapper.save(logininfo);
        // 2.保存 user
        user.setLogininfo_id(logininfo.getId());
        userMapper.save(user);

    }
    private User userDto2User(UserDto userDto) {
        User user = new User();
        user.setUsername(userDto.getPhone());
        user.setPhone(userDto.getPhone());
        String salt = StrUtils.getComplexRandomString(32);
        //使用盐值加密
        String md5Pwd = MD5Utils.encrypByMd5(userDto.getPassword() + salt);
        user.setSalt(salt);
        user.setPassword(md5Pwd);
        return user;
    }

    private Logininfo user2Logininfo(User user) {
        Logininfo logininfo = new Logininfo();
        // 同名原则拷贝
        BeanUtils.copyProperties(user, logininfo);
        logininfo.setType(1);
        return logininfo;
    }
}
