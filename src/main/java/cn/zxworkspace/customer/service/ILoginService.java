package cn.zxworkspace.customer.service;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.customer.dto.LoginDto;

import java.util.Map;

public interface ILoginService {
    Map<String, Object> accountLogin(LoginDto loginDto);

    AjaxResult wechatLogin(String code);

    Map<String, Object> binderWechat(Map<String, String> params);
}
