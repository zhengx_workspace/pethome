package cn.zxworkspace.customer.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.customer.dto.UserDto;

public interface IUserService extends IBaseService<User> {

    void registerByPhone(UserDto dto);
}
