package cn.zxworkspace.customer.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.customer.domain.User;

public interface ILogininfoService extends IBaseService<Logininfo> {

}
