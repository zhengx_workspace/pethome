package cn.zxworkspace.customer.dto;

import lombok.Data;

@Data
public class LoginDto {
    private String username;
    private String password;
    private Integer type=0;
}
