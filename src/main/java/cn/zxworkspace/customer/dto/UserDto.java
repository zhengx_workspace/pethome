package cn.zxworkspace.customer.dto;

import lombok.Data;

@Data
public class UserDto {
    private String password;
    private String confirmPassword;
    private String registerCode;
    private String phone;
}
