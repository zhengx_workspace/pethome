package cn.zxworkspace.customer.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import cn.zxworkspace.org.domain.Employee;
import lombok.Data;

import java.util.Date;

@Data
public class User extends BaseDomain {
    private String username;
    private String email;
    private String phone;
    private String salt;
    private String password;
    private Integer state;
    private Integer age;
    private Date createtime=new Date();
    private String headImg;
    private Long logininfo_id;


}
