package cn.zxworkspace.customer.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

import java.util.Date;

@Data
public class Logininfo extends BaseDomain {
    private String username;
    private String email;
    private String phone;
    private String salt;
    private String password;
    private Integer type=0;
    private Integer disable=1;


}
