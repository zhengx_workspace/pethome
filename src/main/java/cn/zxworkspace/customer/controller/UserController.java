package cn.zxworkspace.customer.controller;

import cn.zxworkspace.basic.exception.BasicException;
import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.customer.dto.UserDto;
import cn.zxworkspace.customer.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;
    /**
     * 手机号注册
     * @return
     */
    @PostMapping("/register/phone")
    public AjaxResult phoneRegister(@RequestBody UserDto dto){
        try {
            userService.registerByPhone(dto);
            return AjaxResult.me();
        } catch (BasicException e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统繁忙请稍后再试！");
        }
    }
}
