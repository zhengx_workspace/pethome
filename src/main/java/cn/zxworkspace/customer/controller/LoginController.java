package cn.zxworkspace.customer.controller;

import cn.zxworkspace.basic.exception.BasicException;
import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.customer.dto.LoginDto;
import cn.zxworkspace.customer.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private ILoginService loginService;

    @PostMapping("/account")
    public AjaxResult accountLogin(@RequestBody LoginDto loginDto){
        try {
            Map<String,Object> map = loginService.accountLogin(loginDto);
            return AjaxResult.me().setResultObj(map);
        } catch (BasicException e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统繁忙请稍后再试！");
        }
    }
    @PostMapping("/wechat")
    public AjaxResult wechatLogin(@RequestBody Map<String,String> params){
        try {
            return loginService.wechatLogin(params.get("code"));
        } catch (BasicException e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统繁忙请稍后再试！");
        }
    }
    @PostMapping("/binder/wechat")
    public AjaxResult binderWechat(@RequestBody Map<String,String> params){
        try {
            Map<String,Object> map = loginService.binderWechat(params);
            return AjaxResult.me().setResultObj(map);
        } catch (BasicException e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统繁忙请稍后再试！");
        }
    }

}
