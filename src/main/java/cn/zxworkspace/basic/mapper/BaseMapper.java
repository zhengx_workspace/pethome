package cn.zxworkspace.basic.mapper;


import cn.zxworkspace.basic.query.BaseQuery;

import java.util.List;

public interface BaseMapper<T> {
    void save(T t);

    void remove(Long id);

    void update(T t);

    T loadById(Long id);
    //查询所有
    List<T> loadAll();

    /**
     * 分页查询
     *
     */
    //查询总条数
    Long queryCount(BaseQuery query);
    //查询当前的页数
    List<T> queryData(BaseQuery query);

    void removeList(List<Long> ids);
}
