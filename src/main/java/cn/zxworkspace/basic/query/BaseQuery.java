package cn.zxworkspace.basic.query;

import lombok.Data;

@Data
public class BaseQuery {
    //当前页条数
    private Integer psize = 50;
    //第几页
    private Integer cpage = 1;
    private String keyword;

    public Integer getBegin(){
        return (this.cpage-1)*this.psize;
    }
}
