package cn.zxworkspace.basic.service.impl;

import cn.zxworkspace.basic.constant.SmsConstant;
import cn.zxworkspace.basic.exception.BasicException;
import cn.zxworkspace.basic.service.IVerifyCodeService;
import cn.zxworkspace.basic.util.StrUtils;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.customer.mapper.UserMapper;
import cn.zxworkspace.customer.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class VerifyCodeServiceImpl implements IVerifyCodeService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public void sendSmsCodeByPhone(Map<String,String> params) {
//        1.校验
        String phone = params.get("phone");
        String type = params.get("type");
        //        1.1 手机号不能为空 phone
        if(StringUtils.isEmpty(phone)|| StringUtils.isEmpty(type)){
            throw new BasicException("手机号为空，请重新输入");
        }
        String businessKey = "";
        if("register".equals(type)){//手机号注册
            //        1.2 手机号不能被注册, 报错 查询 t_user
            User user = userMapper.findByPhone(phone);
            if (user != null) {
                throw new BasicException("用户已存在,请直接登录!!!");
            }
            businessKey = SmsConstant.USER_REGISTER_CONSTANT + phone;
        }else if("binder".equals(type)){//绑定微信
            businessKey = SmsConstant.BUSINESS_BINDER_PREFIX + phone;
        }
        sendCode(businessKey);


    }
    public void sendCode(String businessKey){
        String key = businessKey;
        Object userRegister = redisTemplate.opsForValue().get(key);
//        2.1 如果没有过期    value 9527:1346489765465
        String code ="";
        //如果账户不为空，有这个账户，则需要判断他的验证码是否过期
        if(userRegister!=null){
            String str = (String)userRegister;
            //分割获取value的后面的时间戳
            Long oldTime = Long.valueOf(str.split(":")[1]);
            //2.1.1 判断是否过了1分钟重发时间 1 *60 * 1000
            if(System.currentTimeMillis()-oldTime<=1*60*1000){
                //2.1.1.2 如果没过, 使劲骂....
                throw new BasicException("时间小于1分钟，别瞎搞！");
            }else {
                //2.1.1.1 如果过了 (过了1分钟,还没到3分钟,使用还没过期的验证码)=== code  9527
                code = str.split(":")[0];
            }
        } else {
            // 2.2 如果过期了(或者是第一次来)
            //2.2.1 直接生成新的验证码   ===  code
            code = StrUtils.getComplexRandomString(4);
        }
        String codeValue=code+":"+System.currentTimeMillis();
        //3.保存验证码到redis 直接设置3分钟过期就ok了
        redisTemplate.opsForValue().set(key, codeValue, 3, TimeUnit.MINUTES);
        System.out.println("验证码为："+code);
    }
}
