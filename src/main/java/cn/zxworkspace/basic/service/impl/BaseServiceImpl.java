package cn.zxworkspace.basic.service.impl;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.basic.query.BaseQuery;
import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.org.domain.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class BaseServiceImpl<T> implements IBaseService<T> {
    @Autowired
    private BaseMapper<T> mapper;
    @Override
    @Transactional
    public void add(T t) {
        mapper.save(t);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        mapper.remove(id);
    }

    @Override
    @Transactional
    public void update(T t) {
        mapper.update(t);
    }

    @Override
    public T queryById(Long id) {
        return mapper.loadById(id);
    }

    @Override
    public List<T> queryAll() {
        return mapper.loadAll();
    }

    @Override
    public PageList<T> queryPage(BaseQuery query) {
        //查询当前页数据
        List<T> rows = mapper.queryData(query);
        //查询数据总条数
        Long total = mapper.queryCount(query);
        //封装数据给controller层
        return new PageList<>(total,rows);
    }

    @Override
    public void deleteList(List<Long> ids) {
        mapper.removeList(ids);
    }
}
