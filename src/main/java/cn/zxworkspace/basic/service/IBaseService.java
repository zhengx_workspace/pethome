package cn.zxworkspace.basic.service;

import cn.zxworkspace.basic.query.BaseQuery;
import cn.zxworkspace.basic.util.PageList;


import java.util.List;

public interface IBaseService<T> {
    void add(T t);

    void delete(Long id);

    void update(T t);

    T queryById(Long id);
    //查询所有
    List<T> queryAll();

    /**
     * 分页查询
     *
     */
    //查询总条数
    PageList<T> queryPage(BaseQuery query);

    void deleteList(List<Long> ids);
}
