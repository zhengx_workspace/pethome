package cn.zxworkspace.basic.service;

import java.util.Map;

public interface IVerifyCodeService {
    void sendSmsCodeByPhone(Map<String,String> params);
}
