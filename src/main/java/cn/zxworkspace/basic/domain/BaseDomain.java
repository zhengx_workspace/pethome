package cn.zxworkspace.basic.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseDomain implements Serializable {
    /*主键*/
    private Long id;
}
