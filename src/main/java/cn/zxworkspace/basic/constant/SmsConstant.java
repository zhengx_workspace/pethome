package cn.zxworkspace.basic.constant;

public class SmsConstant {
    //手机注册
    public static final String USER_REGISTER_CONSTANT="user_register_code:";
    public static final String BUSINESS_BINDER_PREFIX="business_binder_prefix:";

    public static final String BUSINESS_LOGIN_PREFIX="business_login_prefix:";


    //  ================= 微信登录相关
    public static final String APPID="wxd853562a0548a7d0";
    public static final String SECRET="4a5d5615f93f24bdba2ba8534642dbb6";

    public static final String GET_TOKEN_URL="https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
    public static final String GET_WXUSER_URL="https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID";

}
