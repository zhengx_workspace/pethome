package cn.zxworkspace.basic.controller;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.FastDfsUtil;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/fastdfs")
public class FastDfsController {

    @PostMapping
    public AjaxResult upload(@RequestPart(required = true,value = "file")MultipartFile file){
        try {
            System.out.println(file.getOriginalFilename() + ":" + file.getSize());
            String fileName = file.getOriginalFilename();
            String extName = fileName.substring(fileName.lastIndexOf(".")+1);
            System.out.println(extName);
            String filePath =  FastDfsUtil.upload(file.getBytes(), extName);
            return AjaxResult.me().setResultObj(filePath); //把上传后的路径返回回去
        } catch (IOException e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("上传失败!"+e.getMessage());
        }
    }
    @DeleteMapping
    public AjaxResult delete(@RequestParam String path){
        try {
            String pathTmp = path.substring(1);
            String groupName =  pathTmp.substring(0, pathTmp.indexOf("/"));
            String fileName = pathTmp.substring(pathTmp.indexOf("/")+1);
            FastDfsUtil.delete(groupName, fileName);

            return AjaxResult.me();//把上传之后的相对路径设置给resultObj属性
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("删除失败!!!");
        }
    }
}
