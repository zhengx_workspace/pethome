package cn.zxworkspace.basic.controller;

import cn.zxworkspace.basic.exception.BasicException;
import cn.zxworkspace.basic.service.IVerifyCodeService;
import cn.zxworkspace.basic.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/verifycode")
public class VerifyCodeController {
    @Autowired
    private IVerifyCodeService verifyCodeService;

    @PostMapping("/sendSmsCode")
    public AjaxResult sendSmsCode(@RequestBody Map<String,String> params){
        try {
            verifyCodeService.sendSmsCodeByPhone(params);
            return AjaxResult.me();
        } catch (BasicException e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统繁忙，稍后再试！");
        }

    }
}
