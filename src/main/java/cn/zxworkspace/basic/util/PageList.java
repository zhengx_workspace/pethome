package cn.zxworkspace.basic.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageList<T> {
    //初始数据总条数
    private Long total = 0L;
    //当前页的数据
    private List<T> rows = new ArrayList<>();
}
