package cn.zxworkspace.basic.util;

import cn.zxworkspace.customer.domain.Logininfo;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 获取当前登录人
 */
public class LoginContext {
    public static Logininfo getCurrentLogininfo(HttpServletRequest request) {
        //获取头消息中的token
        String token = request.getHeader("token");
        if (!StringUtils.isEmpty(token)) {
            //获取spring容器
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
            //获取其中注入的bean。
            RedisTemplate redisTemplate = (RedisTemplate) webApplicationContext.getBean("redisTemplate");
            Object logininfo = redisTemplate.opsForValue().get(token);
            if (logininfo != null) {
                return (Logininfo) logininfo;
            }
        }
        return null;
    }

}
