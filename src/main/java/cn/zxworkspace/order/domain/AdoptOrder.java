package cn.zxworkspace.order.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.org.domain.Shop;
import cn.zxworkspace.pet.domain.Pet;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class AdoptOrder extends BaseDomain {
    private String digest;
    private Integer state;
    private BigDecimal price;
    private String orderSn;
    private String paySn;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date lastPayTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date lastConfirmTime;
    private Long pet_id;
    private Long user_id;
    private Long shop_id;

    private Pet pet;
    private User user;
    private Shop shop;

}
