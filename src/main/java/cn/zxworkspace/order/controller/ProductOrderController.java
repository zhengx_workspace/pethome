package cn.zxworkspace.order.controller;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.LoginContext;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.order.domain.AdoptOrder;
import cn.zxworkspace.order.domain.ProductOrder;
import cn.zxworkspace.order.query.AdoptOrderQuery;
import cn.zxworkspace.order.service.IProductOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/productOrder")
public class ProductOrderController {

    @Autowired
    private IProductOrderService productOrderService;
    @PostMapping("/submit")
    public AjaxResult submit(@RequestBody Map<String,Object> params, HttpServletRequest request){

        try{
            String payData = productOrderService.submit(params, LoginContext.getCurrentLogininfo(request));
            System.out.println(payData);
            return AjaxResult.me().setResultObj(payData);
        }catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("下单失败！"+e.getMessage());
        }

    }

    @PostMapping("/admin")
    public PageList<ProductOrder> admin(@RequestBody AdoptOrderQuery query, HttpServletRequest request){
        return productOrderService.admin(query,LoginContext.getCurrentLogininfo(request));
    }
    @GetMapping("/{productId}")
    public ProductOrder queryById(@PathVariable("productId") Long id){
        return productOrderService.queryById(id);
    }
}
