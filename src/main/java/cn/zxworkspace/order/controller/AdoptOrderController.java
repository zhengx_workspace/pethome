package cn.zxworkspace.order.controller;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.LoginContext;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.order.domain.AdoptOrder;
import cn.zxworkspace.order.mapper.AdoptOrderMapper;
import cn.zxworkspace.order.query.AdoptOrderQuery;
import cn.zxworkspace.order.service.IAdoptOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 领养订单接口
 */
@RestController
@RequestMapping("/adoptOrder")
public class AdoptOrderController {
    @Autowired
    private IAdoptOrderService adoptOrderService;

    @PostMapping("/submit")
    public AjaxResult submit(@RequestBody Map<String,Object> params, HttpServletRequest request){
        try{
            String payData = adoptOrderService.submit(params, LoginContext.getCurrentLogininfo(request));
            System.out.println(payData);
            return AjaxResult.me().setResultObj(payData);
        }catch (Exception e){
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("下单失败！"+e.getMessage());
        }
    }
    @PostMapping("/admin")
    public PageList<AdoptOrder> admin(@RequestBody AdoptOrderQuery query,HttpServletRequest request){
        return adoptOrderService.admin(query,LoginContext.getCurrentLogininfo(request));
    }
    @PostMapping("/user")
    public PageList<AdoptOrder> user(@RequestBody AdoptOrderQuery query,HttpServletRequest request){
        return adoptOrderService.admin(query,LoginContext.getCurrentLogininfo(request));
    }
    @GetMapping("/{orderId}")
    public AdoptOrder queryById(@PathVariable("orderId") Long id){
        return adoptOrderService.queryById(id);
    }
}
