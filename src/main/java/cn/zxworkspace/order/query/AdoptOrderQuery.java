package cn.zxworkspace.order.query;

import cn.zxworkspace.basic.query.BaseQuery;
import lombok.Data;

@Data
public class AdoptOrderQuery extends BaseQuery {
    private Long ShopId;
    private Long UserId;
}
