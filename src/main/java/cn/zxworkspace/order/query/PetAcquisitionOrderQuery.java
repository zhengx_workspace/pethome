package cn.zxworkspace.order.query;

import cn.zxworkspace.basic.query.BaseQuery;
import lombok.Data;

@Data
public class PetAcquisitionOrderQuery extends BaseQuery {
    private Long handlerId;
    private Integer state;
}
