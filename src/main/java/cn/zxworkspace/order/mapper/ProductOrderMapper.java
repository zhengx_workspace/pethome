package cn.zxworkspace.order.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.order.domain.ProductOrder;

public interface ProductOrderMapper extends BaseMapper<ProductOrder> {
}
