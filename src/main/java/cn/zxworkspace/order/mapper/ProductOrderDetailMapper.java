package cn.zxworkspace.order.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.order.domain.ProductOrderDetail;

public interface ProductOrderDetailMapper extends BaseMapper<ProductOrderDetail> {
}
