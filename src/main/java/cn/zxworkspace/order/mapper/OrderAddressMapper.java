package cn.zxworkspace.order.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.order.domain.OrderAddress;

public interface OrderAddressMapper extends BaseMapper<OrderAddress> {
}
