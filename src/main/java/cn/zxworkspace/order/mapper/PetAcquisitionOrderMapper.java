package cn.zxworkspace.order.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.order.domain.PetAcquisitionOrder;

public interface PetAcquisitionOrderMapper extends BaseMapper<PetAcquisitionOrder> {
}
