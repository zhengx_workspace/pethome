package cn.zxworkspace.order.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.order.domain.AdoptOrder;

import java.util.Map;

public interface AdoptOrderMapper extends BaseMapper<AdoptOrder> {

}
