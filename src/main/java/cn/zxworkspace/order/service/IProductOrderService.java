package cn.zxworkspace.order.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.order.domain.ProductOrder;
import cn.zxworkspace.order.query.AdoptOrderQuery;

import java.util.Map;

public interface IProductOrderService extends IBaseService<ProductOrder> {
    String submit(Map<String, Object> params, Logininfo logininfo);

    /**
     * 修改服务订单订单状态
     * 修改支付单状态
     * @param orderId
     */
    void cancelOrder(Long orderId);

    PageList<ProductOrder> admin(AdoptOrderQuery query, Logininfo currentLogininfo);
}
