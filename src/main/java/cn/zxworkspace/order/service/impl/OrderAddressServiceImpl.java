package cn.zxworkspace.order.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.order.domain.OrderAddress;
import cn.zxworkspace.order.service.IOrderAddressService;
import org.springframework.stereotype.Service;

@Service
public class OrderAddressServiceImpl extends BaseServiceImpl<OrderAddress> implements IOrderAddressService {

}
