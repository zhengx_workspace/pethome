package cn.zxworkspace.order.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.basic.util.CodeGenerateUtils;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.customer.domain.UserAddress;
import cn.zxworkspace.customer.mapper.UserAddressMapper;
import cn.zxworkspace.customer.mapper.UserMapper;
import cn.zxworkspace.order.domain.AdoptOrder;
import cn.zxworkspace.order.domain.OrderAddress;
import cn.zxworkspace.order.mapper.AdoptOrderMapper;
import cn.zxworkspace.order.mapper.OrderAddressMapper;
import cn.zxworkspace.order.query.AdoptOrderQuery;
import cn.zxworkspace.order.service.IAdoptOrderService;
import cn.zxworkspace.org.domain.Employee;
import cn.zxworkspace.org.mapper.EmployeeMapper;
import cn.zxworkspace.pay.constants.PayConstants;
import cn.zxworkspace.pay.domain.PayBill;
import cn.zxworkspace.pay.mapper.PayBillMapper;
import cn.zxworkspace.pay.service.IPayBillService;
import cn.zxworkspace.pet.domain.Pet;
import cn.zxworkspace.pet.mapper.PetMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

@Service
public class AdoptOrderServiceImpl extends BaseServiceImpl<AdoptOrder> implements IAdoptOrderService {
    @Autowired
    private PetMapper petMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AdoptOrderMapper adoptOrderMapper;
    @Autowired
    private UserAddressMapper userAddressMapper;
    @Autowired
    private OrderAddressMapper orderAddressMapper;
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private PayBillMapper payBillMapper;
    @Autowired
    private IPayBillService payBillService;
    @Override
    @Transactional
    public String submit(Map<String, Object> params, Logininfo logininfo) {
        //获取前端的4个参数
        Long service_method = Long.valueOf(params.get("service_method").toString());
        Long address_id = Long.valueOf(params.get("address_id").toString());
        Integer pay_method = Integer.valueOf(params.get("pay_method").toString());
        Long pet_id = Long.valueOf(params.get("pet_id").toString());
        //根据前端传来的petid查询所购买的宠物
        Pet pet = petMapper.loadById(pet_id);
        //设置状态为已下架
        pet.setState(0);
        //设置下架时间
        pet.setOffsaletime(new Date());
        //绑定购买人
        User user = userMapper.loadByLogininfoId(logininfo.getId());
        pet.setUser_id(user.getId());
        petMapper.update(pet);
        //3.生成领养订单
        AdoptOrder adoptOrder = initAdoptOrder(pet, user);
        //3.1保存订单
        adoptOrderMapper.save(adoptOrder);
        //3.2订单地址
        //根据前端传的值，查询数据库中的地址。
        UserAddress userAddress = userAddressMapper.loadById(address_id);
        //将这个地址。设置给订单地址表
        OrderAddress orderAddress = initOrderAddress(adoptOrder, userAddress);
        //保存表
        orderAddressMapper.save(orderAddress);
        //生成支付详情单。
        PayBill payBill = initPayBill(pay_method, user, adoptOrder);
        //保存支付详情表
        payBillMapper.save(payBill);
        //丢给service层做判断，在和支付宝接口进行交互
        return payBillService.pay(payBill);
    }

    /**
     * 生成支付详情表
     * @param payMethod  支付方式
     * @param user  用户
     * @param order 订单
     * @return
     */
    private PayBill initPayBill(Integer payMethod, User user, AdoptOrder order) {
        PayBill bill = new PayBill();
        bill.setDigest("[摘要]对领养"+order.getOrderSn()+"支付订单！");
        bill.setMoney(order.getPrice());
        //支付订单唯一标识
        bill.setUnionPaySn(order.getOrderSn());
        bill.setLastPayTime(order.getLastPayTime());
        //支付方式
        bill.setPayChannel(payMethod);
        //设置订单类型
        bill.setBusinessType(PayConstants.LINGYANG_ORDER);
        bill.setBusinessKey(order.getId());
        bill.setUser_id(order.getUser_id());
        bill.setShop_id(order.getShop_id());
        bill.setNickName(user.getUsername());
        return bill;
    }


    /**
     * 分页查询购买订单
     * @param query
     * @param currentLogininfo
     * @return
     */
    @Override
    public PageList<AdoptOrder> admin(AdoptOrderQuery query, Logininfo currentLogininfo) {
        Employee employee = employeeMapper.loadByLogininfoId(currentLogininfo.getId());
        //如果获取的登录对象的店铺id不为空，则根据这个电铺的id查询他所拥有的订单
        if(employee.getShop_id() != null){
            query.setShopId(employee.getShop_id());
        }
        return super.queryPage(query);
    }

    /**
     * 根据订单表和用户地址表，生成一张订单地址表
     * @param order
     * @param address
     * @return
     */
    private OrderAddress initOrderAddress(AdoptOrder order, UserAddress address) {
        OrderAddress orderAddress = new OrderAddress();
        BeanUtils.copyProperties(address, orderAddress);
        orderAddress.setOrder_id(order.getId());
        orderAddress.setOrderSn(order.getOrderSn());
        return orderAddress;
    }

    /**
     * 抽取方法根据购买人和购买的宠物生成一张新表
     * @param pet
     * @param user
     * @return
     */
    private AdoptOrder initAdoptOrder(Pet pet, User user) {
        AdoptOrder order = new AdoptOrder();
        order.setDigest("[摘要]对"+pet.getName()+"领养订单！");
        order.setState(0);//待支付
        order.setPrice(pet.getSaleprice());//售价
        //生成随机订单编号
        String orderSn = CodeGenerateUtils.generateOrderSn(user.getId());
        order.setOrderSn(orderSn);
        order.setPet_id(pet.getId());
        order.setUser_id(user.getId());
        order.setShop_id(pet.getShop_id());
        //最后支付时间
        order.setLastPayTime(new Date(System.currentTimeMillis() + 15*60*1000));
        return order;
    }
}
