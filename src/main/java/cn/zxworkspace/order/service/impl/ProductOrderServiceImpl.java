package cn.zxworkspace.order.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.basic.util.CodeGenerateUtils;
import cn.zxworkspace.basic.util.DistanceUtil;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.customer.domain.UserAddress;
import cn.zxworkspace.customer.mapper.UserAddressMapper;
import cn.zxworkspace.customer.mapper.UserMapper;
import cn.zxworkspace.customer.service.IUserService;
import cn.zxworkspace.order.domain.OrderAddress;
import cn.zxworkspace.order.domain.ProductOrder;
import cn.zxworkspace.order.domain.ProductOrderDetail;
import cn.zxworkspace.order.mapper.OrderAddressMapper;
import cn.zxworkspace.order.mapper.ProductOrderDetailMapper;
import cn.zxworkspace.order.mapper.ProductOrderMapper;
import cn.zxworkspace.order.query.AdoptOrderQuery;
import cn.zxworkspace.order.service.IProductOrderService;
import cn.zxworkspace.org.domain.Employee;
import cn.zxworkspace.org.domain.Shop;
import cn.zxworkspace.org.mapper.EmployeeMapper;
import cn.zxworkspace.org.mapper.ShopMapper;
import cn.zxworkspace.pay.constants.PayConstants;
import cn.zxworkspace.pay.domain.PayBill;
import cn.zxworkspace.pay.mapper.PayBillMapper;
import cn.zxworkspace.pay.service.IPayBillService;
import cn.zxworkspace.product.domain.Product;
import cn.zxworkspace.product.mapper.ProductMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class ProductOrderServiceImpl extends BaseServiceImpl<ProductOrder> implements IProductOrderService {

    @Autowired
    private PayBillMapper payBillMapper;
    @Autowired
    private ShopMapper shopMapper;

    @Autowired
    private IPayBillService payBillService;
    @Autowired
    private ProductOrderMapper productOrderMapper;
    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private UserAddressMapper userAddressMapper;

    @Autowired
    private OrderAddressMapper orderAddressMapper;
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductOrderDetailMapper productOrderDetailMapper;

    @Override
    public String submit(Map<String, Object> params, Logininfo logininfo) {
        Long addressId = Long.parseLong(params.get("address_id").toString());
        Long productId = Long.parseLong(params.get("product_id").toString());
        Integer pay_method = Integer.valueOf(params.get("pay_method").toString());
        Product product = productMapper.loadById(productId);
        UserAddress userAddress = userAddressMapper.loadById(addressId);
        User user = userMapper.loadByLogininfoId(logininfo.getId());
        System.out.println(userAddress);
        //1创建订单
        //1.1 创建订单
        ProductOrder productOrder = getProductOrder(params,userAddress,product,user);
        productOrderMapper.save(productOrder);
        //1.2 保存关联表 orderAdress productOrderDetail

        System.out.println(productOrder.getId());
        OrderAddress orderAddress = userAddress2orderAddress(userAddress);
        orderAddress.setOrder_id(productOrder.getId());
        orderAddress.setOrderSn(productOrder.getOrderSn());
        orderAddressMapper.save(orderAddress);

        ProductOrderDetail productOrderDetail = product2productOrderDetail(product);
        productOrderDetail.setOrder_id(productOrder.getId());
        productOrderDetailMapper.save(productOrderDetail);

        // 2 创建支付单
        PayBill payBill = order2PayBill(pay_method, productOrder, user);
        payBillMapper.save(payBill);

        // 3 订单倒计时取消处理
//        QuartzJobInfo info = new QuartzJobInfo();
//        info.setType(JobTypeConsts.WAIT_PRODUCT_ORDER_PAY_CANCEL_JOB);
//        String jobName = "ProductOrderCancelJob"+productOrder.getId();
//        Map<String, Object> infoParams = new HashMap<>();
//        infoParams.put("orderId",productOrder.getId());
//        Date fireDate = productOrder.getLastPayTime();
//        info.setJobName(jobName);
//        info.setParams(infoParams);
//        info.setFireDate(fireDate);
//        quartzService.addJob(info);
        // 4 跳转到支付页面,通过调用支付接口，要把这个数据返回前台，前台处理后跳转支付宝支付页面
        String payData = payBillService.pay(payBill);
        return payData;
    }



//    @Autowired
//    private IQuartzService quartzService;
//    @Override
//    public String submit(Map<String, Object> params, User user) {
//
//    }
//
    @Override
    public void cancelOrder(Long orderId) {
        ProductOrder productOrder = productOrderMapper.loadById(orderId);
        if(productOrder.getState()!=-1){
            productOrder.setState(-1);
            productOrderMapper.update(productOrder);
        }

        String paySn = productOrder.getPaySn();
        PayBill payBill = payBillMapper.loadByUnionPaySn(paySn);
        if (payBill.getState()!=-1){
            payBill.setState(-1);
            payBillMapper.update(payBill);
        }

    }

    @Override
    public PageList<ProductOrder> admin(AdoptOrderQuery query, Logininfo logininfo) {
        Employee employee = employeeMapper.loadByLogininfoId(logininfo.getId());
        if(employee.getShop_id()!=null){
            query.setShopId(employee.getShop_id());
        }
        return super.queryPage(query);
    }

    private PayBill order2PayBill(Integer pay_method, ProductOrder productOrder,User user) {
        PayBill payBill = new PayBill();
        payBill.setDigest(productOrder.getDigest());
        payBill.setMoney(productOrder.getPrice());
        payBill.setUnionPaySn(productOrder.getPaySn());
        payBill.setState(1);
        payBill.setLastPayTime(productOrder.getLastPayTime());
        payBill.setPayChannel(pay_method);
        payBill.setBusinessType(PayConstants.FUWU_ORDER);
        payBill.setBusinessKey(productOrder.getId());
        payBill.setCreateTime(new Date());
        payBill.setUser_id(productOrder.getUser_id());
        payBill.setNickName(user.getUsername());
        payBill.setShop_id(productOrder.getShop_id());
        return payBill;
    }

    private ProductOrderDetail product2productOrderDetail(Product product) {
        ProductOrderDetail productOrderDetail = new ProductOrderDetail();
        BeanUtils.copyProperties(product,productOrderDetail);
        return productOrderDetail;
    }

    private OrderAddress userAddress2orderAddress(UserAddress userAddress) {
        OrderAddress orderAddress = new OrderAddress();
        BeanUtils.copyProperties(userAddress,orderAddress);
        orderAddress.setContacts(userAddress.getContacts());
        return orderAddress;
    }

    private ProductOrder getProductOrder(Map<String, Object> params,UserAddress userAddress,Product product, User user) {
        Long productNum = Long.parseLong(params.get("product_num").toString());
        ProductOrder productOrder = new ProductOrder();

        productOrder.setDigest("[服务订单]购买"+productNum+"次"+product.getName()+"服务！");
        productOrder.setState(1);
        BigDecimal price  = product.getSaleprice().multiply(new BigDecimal(1))
                .multiply(new BigDecimal(productNum));
        productOrder.setPrice(price);
        productOrder.setOrderSn(CodeGenerateUtils.generateOrderSn(user.getId()));
        Date lastPayTime = new Date(System.currentTimeMillis()+60*1000); //当前时间+30分钟
        productOrder.setLastPayTime(lastPayTime);
        productOrder.setProduct_id(product.getId());
        productOrder.setUser_id(user.getId());
        Shop nearestShop = DistanceUtil.getNearestShop(DistanceUtil.getPoint(userAddress.getFullAddress()), shopMapper.loadAll());
        productOrder.setShop_id(nearestShop.getId());
        productOrder.setPaySn(CodeGenerateUtils.generateUnionPaySn());
        return productOrder;
    }

}
