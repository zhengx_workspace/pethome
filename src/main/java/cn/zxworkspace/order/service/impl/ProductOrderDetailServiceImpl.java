package cn.zxworkspace.order.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.order.domain.ProductOrderDetail;
import cn.zxworkspace.order.service.IProductOrderDetailService;
import org.springframework.stereotype.Service;

@Service
public class ProductOrderDetailServiceImpl extends BaseServiceImpl<ProductOrderDetail> implements IProductOrderDetailService {
}
