package cn.zxworkspace.order.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.order.domain.PetAcquisitionOrder;
import cn.zxworkspace.order.service.IPetAcquisitionOrderService;
import org.springframework.stereotype.Service;

@Service
public class PetAcquistionOrderServiceImpl extends BaseServiceImpl<PetAcquisitionOrder> implements IPetAcquisitionOrderService {
}
