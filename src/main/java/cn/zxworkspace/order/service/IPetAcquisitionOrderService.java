package cn.zxworkspace.order.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.order.domain.PetAcquisitionOrder;

public interface IPetAcquisitionOrderService extends IBaseService<PetAcquisitionOrder> {
}
