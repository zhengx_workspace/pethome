package cn.zxworkspace.order.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.order.domain.AdoptOrder;
import cn.zxworkspace.order.query.AdoptOrderQuery;

import java.util.Map;

public interface IAdoptOrderService extends IBaseService<AdoptOrder> {
    String submit(Map<String, Object> params, Logininfo currentLogininfo);

    PageList<AdoptOrder> admin(AdoptOrderQuery query, Logininfo currentLogininfo);
}
