package cn.zxworkspace.order.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.order.domain.OrderAddress;

public interface IOrderAddressService extends IBaseService<OrderAddress> {
}
