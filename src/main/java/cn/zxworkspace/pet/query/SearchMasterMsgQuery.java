package cn.zxworkspace.pet.query;


import cn.zxworkspace.basic.query.BaseQuery;
import lombok.Data;

@Data
public class SearchMasterMsgQuery extends BaseQuery {
    private Integer state;
    private Long userId;
    private Long shopId;
}
