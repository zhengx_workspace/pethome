package cn.zxworkspace.pet.query;

import cn.zxworkspace.basic.query.BaseQuery;
import lombok.Data;

@Data
public class PetTypeQuery extends BaseQuery {
}
