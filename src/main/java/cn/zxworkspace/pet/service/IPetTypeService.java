package cn.zxworkspace.pet.service;


import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.pet.domain.PetType;

import java.util.List;

public interface IPetTypeService extends IBaseService<PetType> {

}
