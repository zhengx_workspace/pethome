package cn.zxworkspace.pet.service;



import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.pet.domain.Pet;

import java.util.List;

public interface IPetService extends IBaseService<Pet> {

    /**
     * 下架宠物
     * @param ids
     */
    void offSale(List<Long> ids);

    /**
     * 下架宠物
     * @param ids
     */
    void onSale(List<Long> ids);

    /**
     * 根据id来查询详情
     * @param id
     * @return
     */
    Pet loadByIdDetail(Long id);

    void adopt(Long id, Logininfo currentLogininfo);
}
