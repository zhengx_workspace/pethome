package cn.zxworkspace.pet.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.basic.util.DistanceUtil;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.basic.util.Point;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.customer.mapper.UserMapper;
import cn.zxworkspace.org.domain.Employee;
import cn.zxworkspace.org.domain.Shop;
import cn.zxworkspace.org.mapper.EmployeeMapper;
import cn.zxworkspace.org.mapper.ShopMapper;
import cn.zxworkspace.pet.domain.Pet;
import cn.zxworkspace.pet.domain.SearchMasterMsg;
import cn.zxworkspace.pet.mapper.SearchMasterMsgMapper;
import cn.zxworkspace.pet.query.SearchMasterMsgQuery;
import cn.zxworkspace.pet.service.IPetService;
import cn.zxworkspace.pet.service.ISearchMasterMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchMasterMsgServiceImpl extends BaseServiceImpl<SearchMasterMsg> implements ISearchMasterMsgService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ShopMapper shopMapper;
    @Autowired
    private SearchMasterMsgMapper searchMasterMsgMapper;
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private IPetService petService;
    @Override
    public void publish(SearchMasterMsg msg, Logininfo logininfo) {
        //绑定当前登陆人
        //根据登录人的id来获取user对象
        User user = userMapper.loadByLogininfoId(logininfo.getId());
        //设置表的user_id为这个user的id
        msg.setUser_id(user.getId());
        //根据表单传来的地址。用工具类的获取的点
        Point point = DistanceUtil.getPoint(msg.getAddress());
        //查出最近的宠物店铺
        Shop nearestShop = DistanceUtil.getNearestShop(point, shopMapper.loadAll());
        System.out.println(nearestShop);
        //给寻主表单设置shop_id
        msg.setShop_id(nearestShop.getId());
        searchMasterMsgMapper.save(msg);


    }

    /**
     * 登陆人用户查询发布的寻主
     * @param query
     * @param logininfo
     * @return
     */
    @Override
    public PageList<SearchMasterMsg> user(SearchMasterMsgQuery query, Logininfo logininfo) {
        //根据登录i人的id查出user表。
        User user = userMapper.loadByLogininfoId(logininfo.getId());
        if(user != null){
            //给条件查询中的userid赋值
            query.setUserId(user.getId());
        }
        return super.queryPage(query);
    }
    /**
     * 未处理的寻主消息
     * @param query
     * @param logininfo
     * @return
     */
    @Override
    public PageList<SearchMasterMsg> pending(SearchMasterMsgQuery query, Logininfo logininfo) {
        query.setState(0);//到处理寻主消息
        Employee employee = employeeMapper.loadByLogininfoId(logininfo.getId());
        if(employee.getShop_id() != null){
            query.setShopId(employee.getShop_id());//查询某一个店铺的数据
        }
        return super.queryPage(query);
    }

    @Override
    public PageList<SearchMasterMsg> processed(SearchMasterMsgQuery query, Logininfo logininfo) {
        query.setState(1);//已处理寻主消息
        Employee employee = employeeMapper.loadByLogininfoId(logininfo.getId());
        if(employee.getShop_id() != null){
            query.setShopId(employee.getShop_id());//查询某一个店铺的数据
        }
        return super.queryPage(query);
    }

    @Override
    public void handle(Pet pet, Logininfo logininfo) {
        System.out.println(pet);
        //修改宠物的上架状态
        searchMasterMsgMapper.updateStateBymsgId(pet.getSearch_master_msg_id());
        //将宠物的基本信息存入表单。
        //获取表单中的详细信息。
        petService.add(pet);

    }
//    private PetAcquisitionOrder pet2order(Pet pet, SearchMasterMsg adopt) {
//        PetAcquisitionOrder order = new PetAcquisitionOrder();
//        order.setDigest("[摘要]对"+pet.getName()+"收购订单！");
//        order.setState(0);//待支付
//        order.setPrice(pet.getCostprice());
//        order.setAddress(adopt.getAddress());
//        String orderSn = CodeGenerateUtils.generateOrderSn(adopt.getUser_id());
//        order.setOrderSn(orderSn);
//        order.setPet_id(pet.getId());
//        order.setUser_id(adopt.getUser_id());
//        order.setPaytype(0);
//        order.setShop_id(pet.getShop_id());
//        order.setEmp_id(EmployeeContext.getLoginEployee().getId());
//        return order;
//    }
}
