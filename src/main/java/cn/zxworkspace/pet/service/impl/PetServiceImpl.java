package cn.zxworkspace.pet.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.customer.mapper.UserMapper;
import cn.zxworkspace.pet.domain.Pet;
import cn.zxworkspace.pet.domain.PetDetail;
import cn.zxworkspace.pet.mapper.PetDetailMapper;
import cn.zxworkspace.pet.mapper.PetMapper;
import cn.zxworkspace.pet.service.IPetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PetServiceImpl  extends BaseServiceImpl<Pet> implements IPetService {

    @Autowired
    private PetMapper petMapper;
    @Autowired
    private PetDetailMapper petDetailMapper;
    @Autowired
    private UserMapper userMapper;

    /**
     * 重写方法：保存pet信息时，级联保存petDeatil。
     * @param pet
     */
    @Override
    public void add(Pet pet) {
        //1.保存pet信息
        petMapper.save(pet);//返回自增ID
        //2.保存petDetail信息
        PetDetail detail = pet.getDetail();
        if(detail != null){
            detail.setPet_id(pet.getId());
            petDetailMapper.save(detail);
        }
    }

    /**
     * 下架
     * @param ids
     */
    @Override
    public void offSale(List<Long> ids) {
        Map<String,Object> map = new HashMap<>();
        map.put("ids", ids);
        map.put("offsaletime", new Date());
        petMapper.offSale(map);
    }

    /**
     * 上架
     * @param ids
     */
    @Override
    public void onSale(List<Long> ids) {
        Map<String,Object> map = new HashMap<>();
        map.put("ids", ids);
        map.put("onsaletime", new Date());
        petMapper.onSale(map);
    }

    @Override
    public Pet loadByIdDetail(Long id) {
        return petMapper.loadByIdDetail(id);
    }

    /**
     * 买宠物
     * @param id
     * @param logininfo
     */
    @Override
    public void adopt(Long id, Logininfo logininfo) {
        Pet pet = petMapper.loadById(id);
        //1.下架宠物
        pet.setState(0);
        pet.setOffsaletime(new Date());
        //2.绑定宠物领养人
        User user = userMapper.loadByLogininfoId(logininfo.getId());
        pet.setUser_id(user.getId());
        petMapper.update(pet);
    }
}
