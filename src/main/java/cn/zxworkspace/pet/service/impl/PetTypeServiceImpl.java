package cn.zxworkspace.pet.service.impl;


import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.pet.domain.PetType;
import cn.zxworkspace.pet.service.IPetTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PetTypeServiceImpl extends BaseServiceImpl<PetType> implements IPetTypeService {


}
