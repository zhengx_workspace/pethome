package cn.zxworkspace.pet.service.impl;


import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.pet.domain.PetDetail;
import cn.zxworkspace.pet.mapper.PetDetailMapper;
import cn.zxworkspace.pet.service.IPetDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PetDetailServiceImpl extends BaseServiceImpl<PetDetail> implements IPetDetailService {
    @Autowired
    private PetDetailMapper petDetailMapper;

    /**
     * 根据宠物id来获取详情
     * @param petId
     * @return
     */
    @Override
    public PetDetail getByPetId(Long petId) {
        return petDetailMapper.loadByPetId(petId);
    }
}
