package cn.zxworkspace.pet.service;


import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.pet.domain.PetDetail;

public interface IPetDetailService extends IBaseService<PetDetail> {
    PetDetail getByPetId(Long petId);
}
