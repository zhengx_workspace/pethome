package cn.zxworkspace.pet.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.pet.domain.Pet;
import cn.zxworkspace.pet.domain.SearchMasterMsg;
import cn.zxworkspace.pet.query.SearchMasterMsgQuery;

public interface ISearchMasterMsgService extends IBaseService<SearchMasterMsg> {

    void publish(SearchMasterMsg msg, Logininfo currentLogininfo);

    PageList<SearchMasterMsg> user(SearchMasterMsgQuery query, Logininfo currentLogininfo);

    PageList<SearchMasterMsg> pending(SearchMasterMsgQuery query, Logininfo currentLogininfo);

    PageList<SearchMasterMsg> processed(SearchMasterMsgQuery query, Logininfo currentLogininfo);

    void handle(Pet pet, Logininfo currentLogininfo);
}
