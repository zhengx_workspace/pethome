package cn.zxworkspace.pet.controller;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.LoginContext;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.pet.domain.Pet;
import cn.zxworkspace.pet.domain.SearchMasterMsg;
import cn.zxworkspace.pet.query.PetQuery;
import cn.zxworkspace.pet.service.IPetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/pet")
public class PetController {

    @Autowired
    private IPetService petService;

    /**
     * 领养宠物
     * @param id 哪一个宠物
     * @return
     */
    @GetMapping("/adopt/{id}")
    public AjaxResult adopt(@PathVariable("id") Long id, HttpServletRequest request){
        try {
            petService.adopt(id, LoginContext.getCurrentLogininfo(request));
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me()
                    .setSuccess(false)
                    .setMessage("系统繁忙,稍后重试");
        }
    }

    //获取一条数据   GET
    @GetMapping("/{id}")
    public Pet findById(@PathVariable("id") Long id){
        return petService.loadByIdDetail(id);
    }


    /**
     * 分页加高级查询
     * @param query
     * @return
     */
    @PostMapping
    public PageList<Pet> getAll(@RequestBody PetQuery query){
        System.out.println(query);
        return petService.queryPage(query);
    }

    /**
     * 下架
     * @param ids
     * @return
     */
    @PostMapping("/offsale")
    public AjaxResult offSale(@RequestBody List<Long> ids){
        try {
            petService.offSale(ids);
            return AjaxResult.me();
        } catch (RuntimeException e) {//我们应该还有自己抛出的业务异常
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        } catch (Exception e) {//系统级别的错误  我们应该还有自己抛出的业务异常
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统繁忙,稍后重试");
        }
    }

    /**
     * 上架
     * @param ids
     * @return
     */
    @PostMapping("/onsale")
    public AjaxResult onSale(@RequestBody List<Long> ids){
        try {
            petService.onSale(ids);
            return AjaxResult.me();
        } catch (RuntimeException e) {//我们应该还有自己抛出的业务异常
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        } catch (Exception e) {//系统级别的错误  我们应该还有自己抛出的业务异常
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统繁忙,稍后重试");
        }
    }

}
