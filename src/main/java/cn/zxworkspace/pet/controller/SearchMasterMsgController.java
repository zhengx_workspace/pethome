package cn.zxworkspace.pet.controller;

import cn.zxworkspace.basic.exception.BasicException;
import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.LoginContext;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.pet.domain.Pet;
import cn.zxworkspace.pet.domain.SearchMasterMsg;
import cn.zxworkspace.pet.query.SearchMasterMsgQuery;
import cn.zxworkspace.pet.service.ISearchMasterMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/searchMasterMsg")
public class SearchMasterMsgController {
    @Autowired
    private ISearchMasterMsgService seachMasterMsgService;


    /**
     * 处理寻主消息(未处理的消息。state为0.处理之后数据库将state转为1.前台页面在未处理列表中不显示）
     * @param pet 表单中的字段和pet对象中的字段基本一样。
     * @param request
     * @return
     */
    @PutMapping("/handle")
    public AjaxResult handle(@RequestBody Pet pet, HttpServletRequest request){
        try {
            seachMasterMsgService.handle(pet, LoginContext.getCurrentLogininfo(request));
            return AjaxResult.me();
        } catch (BasicException e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统异常！");
        }
    }



    @GetMapping
    public List<SearchMasterMsg> getAll(){
        return seachMasterMsgService.queryAll();
    }

    /**
     * 发布寻主消息
     * @param msg
     * @param request
     * @return
     */
    @PostMapping("/publish")
    public AjaxResult publish(@RequestBody SearchMasterMsg msg, HttpServletRequest request){
        try {
            seachMasterMsgService.publish(msg, LoginContext.getCurrentLogininfo(request));
            return AjaxResult.me();
        } catch (BasicException e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统异常！");
        }
    }
    //1.用户查询自己的寻主消息
    @PostMapping("/user")
    public PageList<SearchMasterMsg> user(@RequestBody SearchMasterMsgQuery query, HttpServletRequest request){
        return seachMasterMsgService.user(query,LoginContext.getCurrentLogininfo(request));
    }

    //2.查询待处理寻主消息
    @PostMapping("/pending")
    public PageList<SearchMasterMsg> pending(@RequestBody SearchMasterMsgQuery query,HttpServletRequest request){
        query.setState(0);//待处理
        return seachMasterMsgService.pending(query,LoginContext.getCurrentLogininfo(request));
    }

    //3.查询已处理寻主消息
    @PostMapping("/processed")
    public PageList<SearchMasterMsg> processed(@RequestBody SearchMasterMsgQuery query,HttpServletRequest request){
        query.setState(1);//待处理
        return seachMasterMsgService.processed(query,LoginContext.getCurrentLogininfo(request));
    }



}
