package cn.zxworkspace.pet.mapper;


import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.pet.domain.PetType;

import java.util.Map;

public interface PetTypeMapper extends BaseMapper<PetType> {
}
