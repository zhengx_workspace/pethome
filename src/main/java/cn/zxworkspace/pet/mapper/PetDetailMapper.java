package cn.zxworkspace.pet.mapper;


import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.pet.domain.PetDetail;

import java.io.Serializable;

public interface PetDetailMapper extends BaseMapper<PetDetail> {
    PetDetail loadByPetId(Long productId);

    void removeByPetId(Serializable id);

    void updateByPetId(PetDetail detail);
}
