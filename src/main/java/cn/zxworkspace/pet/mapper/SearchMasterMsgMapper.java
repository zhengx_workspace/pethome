package cn.zxworkspace.pet.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.pet.domain.SearchMasterMsg;

public interface SearchMasterMsgMapper extends BaseMapper<SearchMasterMsg> {
    void updateStateBymsgId(Long search_master_msg_id);
}
