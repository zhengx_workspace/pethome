package cn.zxworkspace.pet.mapper;



import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.pet.domain.Pet;

import java.util.Map;

public interface PetMapper extends BaseMapper<Pet> {
    void onSale(Map<String, Object> params);

    void offSale(Map<String, Object> params);

    Pet loadByIdDetail(Long id);
}
