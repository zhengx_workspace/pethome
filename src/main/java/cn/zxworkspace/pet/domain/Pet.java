package cn.zxworkspace.pet.domain;


import cn.zxworkspace.basic.domain.BaseDomain;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.org.domain.Shop;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Pet extends BaseDomain {
    private String name;
    private String resources;
    private BigDecimal saleprice;
    private Date offsaletime;
    private Date onsaletime;
    private Integer state =0;
    private BigDecimal costprice;
    private Date createtime;
    private Long type_id;
    private PetType type;
    private Long shop_id;
    private Shop shop;
    private Long user_id;
    private User user;
    private Long search_master_msg_id;

    private PetDetail detail = new PetDetail();

}
