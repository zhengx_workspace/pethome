package cn.zxworkspace.pet.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

@Data
public class PetType extends BaseDomain {
    private String name;
    private String description;
    private Long pid;

}
