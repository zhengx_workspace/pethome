package cn.zxworkspace.pet.domain;


import cn.zxworkspace.basic.domain.BaseDomain;
import cn.zxworkspace.customer.domain.User;
import cn.zxworkspace.org.domain.Shop;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SearchMasterMsg extends BaseDomain {
    private String name;
    private BigDecimal price;
    private Integer age;
    private Integer gender;
    private String coat_color;
    private String resources;
    private Long pet_type;
    private PetType type;
    private String address;
    private String title;
    private Integer state = 0;
    private Long user_id;
    private User user;
    private Long shop_id;
    private Shop shop;

}
