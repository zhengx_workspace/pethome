package cn.zxworkspace.pet.domain;


import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

@Data
public class PetDetail extends BaseDomain {
    private Long          pet_id;
    private String adoptNotice;
    private String         intro;
}
