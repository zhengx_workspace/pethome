package cn.zxworkspace.product.controller;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.pet.domain.Pet;
import cn.zxworkspace.pet.query.PetQuery;
import cn.zxworkspace.pet.service.IPetService;
import cn.zxworkspace.product.domain.Product;
import cn.zxworkspace.product.query.ProductQuery;
import cn.zxworkspace.product.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private IProductService productService;

    //获取一条数据   GET
    @GetMapping("/{id}")
    public Product findById(@PathVariable("id") Long id){
        return productService.loadByIdDetail(id);
    }

    /**
     * 新增服务
     */
    @PutMapping
    public AjaxResult put(@RequestBody Product product){
        try {
            productService.add(product);
            return AjaxResult.me();
        } catch (RuntimeException e) {//我们应该还有自己抛出的业务异常
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        } catch (Exception e) {//系统级别的错误  我们应该还有自己抛出的业务异常
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统繁忙,稍后重试");
        }
    }


    /**
     * 分页加高级查询
     * @param query
     * @return
     */
    @PostMapping
    public PageList<Product> getAll(@RequestBody ProductQuery query){
        System.out.println(query);
        return productService.queryPage(query);
    }
    /**
     * 上架
     * @param ids
     * @return
     */
    @PostMapping("/onsale")
    public AjaxResult onSale(@RequestBody List<Long> ids){
        try {
            productService.onSale(ids);
            return AjaxResult.me();
        } catch (RuntimeException e) {//我们应该还有自己抛出的业务异常
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        } catch (Exception e) {//系统级别的错误  我们应该还有自己抛出的业务异常
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统繁忙,稍后重试");
        }
    }
    /**
     * 下架
     * @param ids
     * @return
     */
    @PostMapping("/offsale")
    public AjaxResult offSale(@RequestBody List<Long> ids){
        try {
            productService.offSale(ids);
            return AjaxResult.me();
        } catch (RuntimeException e) {//我们应该还有自己抛出的业务异常
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
        } catch (Exception e) {//系统级别的错误  我们应该还有自己抛出的业务异常
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("系统繁忙,稍后重试");
        }
    }
}
