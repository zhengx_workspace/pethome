package cn.zxworkspace.product.mapper;


import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.pet.domain.PetDetail;
import cn.zxworkspace.product.domain.Product;
import cn.zxworkspace.product.domain.ProductDetail;

import java.io.Serializable;

public interface ProductDetailMapper extends BaseMapper<ProductDetail> {

    void removeByPetId(Serializable id);

    void updateByPetId(PetDetail detail);
}
