package cn.zxworkspace.product.mapper;



import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.pet.domain.Pet;
import cn.zxworkspace.product.domain.Product;

import java.util.Map;

public interface ProductMapper extends BaseMapper<Product> {
    void onSale(Map<String, Object> params);

    void offSale(Map<String, Object> params);

    Product loadDetailById(Long id);
}
