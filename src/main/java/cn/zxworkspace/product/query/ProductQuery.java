package cn.zxworkspace.product.query;

import cn.zxworkspace.basic.query.BaseQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ProductQuery extends BaseQuery {
    private Integer state;

    @Override
    public String toString() {
        return "PetQuery{" +
                "cpage=" + getCpage() +
                ", psize=" + getPsize() +
                ", begin=" + getBegin() +
                ", state=" + state +
                '}';
    }
}
