package cn.zxworkspace.product.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;


@Data
public class ProductDetail extends BaseDomain {
    private Long  product_id;
    private String orderNotice;//预约须知
    private String intro;//简介
}
