package cn.zxworkspace.product.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import cn.zxworkspace.org.domain.Shop;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;


import java.math.BigDecimal;
import java.util.Date;

@Data
public class Product extends BaseDomain {
    private Long salecount;
    private String name;
    private String resources;
    private BigDecimal saleprice;
    @JsonFormat(pattern= "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date offsaletime;
    @JsonFormat(pattern= "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date onsaletime;
    private Integer state=0;
    private String costprice;
    private Date createtime = new Date();
    private Long shop_id;
    private Shop shop;
    private ProductDetail detail = new ProductDetail();

}
