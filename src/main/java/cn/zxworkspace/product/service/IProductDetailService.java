package cn.zxworkspace.product.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.product.domain.Product;
import cn.zxworkspace.product.domain.ProductDetail;

public interface IProductDetailService extends IBaseService<ProductDetail> {
}
