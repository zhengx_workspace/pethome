package cn.zxworkspace.product.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.product.domain.Product;

import java.util.List;

public interface IProductService extends IBaseService<Product> {
    void onSale(List<Long> ids);

    void offSale(List<Long> ids);

    Product loadByIdDetail(Long id);
}
