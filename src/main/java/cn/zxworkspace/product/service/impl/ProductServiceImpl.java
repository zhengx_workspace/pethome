package cn.zxworkspace.product.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.product.domain.Product;
import cn.zxworkspace.product.domain.ProductDetail;
import cn.zxworkspace.product.mapper.ProductDetailMapper;
import cn.zxworkspace.product.mapper.ProductMapper;
import cn.zxworkspace.product.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements IProductService {
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductDetailMapper productDetailMapper;

    /**
     * 上架
     * @param ids
     */
    @Override
    public void onSale(List<Long> ids) {
        Map<String,Object> map = new HashMap<>();
        map.put("ids", ids);
        map.put("onsaletime", new Date());
        productMapper.onSale(map);
    }

    @Override
    public void offSale(List<Long> ids) {
        Map<String,Object> map = new HashMap<>();
        map.put("ids", ids);
        map.put("offsaletime", new Date());
        productMapper.offSale(map);
    }

    @Override
    public Product loadByIdDetail(Long id) {
        return productMapper.loadDetailById(id);
    }

    @Override
    public void add(Product product) {
        productMapper.save(product);
        ProductDetail detail = product.getDetail();
        if(detail!=null){
            detail.setProduct_id(product.getId());
            productDetailMapper.save(detail);
        }
    }
}
