package cn.zxworkspace.product.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.product.domain.ProductDetail;
import cn.zxworkspace.product.service.IProductDetailService;
import org.springframework.stereotype.Service;

@Service
public class ProductDetailServiceImpl extends BaseServiceImpl<ProductDetail> implements IProductDetailService {
}
