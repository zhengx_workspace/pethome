package cn.zxworkspace.pay.domain;


import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class PayBill extends BaseDomain {
    private String digest;
    private BigDecimal money;
    private String unionPaySn;
    private Integer state = 0;// 0待支付1 已支付-1 取消
    private Date lastPayTime;
    private Integer payChannel; //0 余额 1 支付宝 2 微信 3 银联
    private String businessType;
    private Long businessKey;
    private Date updateTime = new Date();
    private Date createTime;
    private Long user_id;
    private Long shop_id;
    private String nickName;

}
