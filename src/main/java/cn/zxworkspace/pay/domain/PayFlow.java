package cn.zxworkspace.pay.domain;


import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class PayFlow extends BaseDomain {

    private String createTime;
    private Long user_id;
    private String nickName;
    private BigDecimal money;
    private Integer type;
    private String businessType;
    private Long businessKey;
    private String businessName;
    private Integer payChannel;
    private String payChannelName;
    private String note;
    private String digest;
    private String unionPaySeq;

}
