package cn.zxworkspace.pay.domain;


import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

@Data
public class AlipayInfo extends BaseDomain {
    private String merchant_private_key;
    private String appid;
    private String alipay_public_key;
    private Long shop_id;
    private String shopName;

}
