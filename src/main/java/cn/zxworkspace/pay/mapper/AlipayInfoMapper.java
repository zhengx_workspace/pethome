package cn.zxworkspace.pay.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.pay.domain.AlipayInfo;

public interface AlipayInfoMapper extends BaseMapper<AlipayInfo> {
    AlipayInfo loadByShopId(Long shop_id);
}
