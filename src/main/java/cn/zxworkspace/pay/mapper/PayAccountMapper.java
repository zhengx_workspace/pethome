package cn.zxworkspace.pay.mapper;


import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.pay.domain.PayAccount;

public interface PayAccountMapper extends BaseMapper<PayAccount> {
}
