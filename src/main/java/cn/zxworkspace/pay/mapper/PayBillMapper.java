package cn.zxworkspace.pay.mapper;


import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.pay.domain.PayBill;

public interface PayBillMapper extends BaseMapper<PayBill> {
    PayBill loadByUnionPaySn(String unionPaySn);
}
