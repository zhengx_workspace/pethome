package cn.zxworkspace.pay.mapper;


import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.pay.domain.PayFlow;

public interface PayFlowMapper extends BaseMapper<PayFlow> {
}
