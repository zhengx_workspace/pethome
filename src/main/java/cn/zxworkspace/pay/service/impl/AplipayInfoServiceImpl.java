package cn.zxworkspace.pay.service.impl;


import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.pay.domain.AlipayInfo;
import cn.zxworkspace.pay.mapper.AlipayInfoMapper;
import cn.zxworkspace.pay.service.IAlipayInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AplipayInfoServiceImpl extends BaseServiceImpl<AlipayInfo> implements IAlipayInfoService {
    @Autowired
    private AlipayInfoMapper alipayInfoMapper;
    @Override
    public AlipayInfo getByShopId(Long shop_id) {
        return alipayInfoMapper.loadByShopId(shop_id);
    }
}
