package cn.zxworkspace.pay.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.pay.domain.PayAccount;
import cn.zxworkspace.pay.service.IPayAccountService;
import org.springframework.stereotype.Service;

@Service
public class PayAccountServiceImpl extends BaseServiceImpl<PayAccount> implements IPayAccountService {
}
