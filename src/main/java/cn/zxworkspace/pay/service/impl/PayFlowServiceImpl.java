package cn.zxworkspace.pay.service.impl;


import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.pay.domain.PayFlow;
import cn.zxworkspace.pay.service.IPayFlowService;
import org.springframework.stereotype.Service;

@Service
public class PayFlowServiceImpl extends BaseServiceImpl<PayFlow> implements IPayFlowService {
}
