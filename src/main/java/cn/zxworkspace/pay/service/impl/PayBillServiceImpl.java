package cn.zxworkspace.pay.service.impl;


import cn.zxworkspace.basic.exception.BasicException;
import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.order.mapper.AdoptOrderMapper;
import cn.zxworkspace.order.service.IProductOrderService;
import cn.zxworkspace.pay.domain.AlipayInfo;
import cn.zxworkspace.pay.domain.PayBill;
import cn.zxworkspace.pay.mapper.AlipayInfoMapper;
import cn.zxworkspace.pay.mapper.PayBillMapper;
import cn.zxworkspace.pay.service.IPayBillService;
import cn.zxworkspace.pay.utils.AlipayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayBillServiceImpl extends BaseServiceImpl<PayBill> implements IPayBillService {

    @Autowired
    private AlipayInfoMapper alipayInfoMapper;

    @Autowired
    private PayBillMapper payBillMapper;

    @Autowired
    private IProductOrderService productOrderService;

    @Override
    public String pay(PayBill billTmp) {
        if(billTmp==null){
            throw new BasicException("对象为空。是不是没保存");
        }
        PayBill bill = payBillMapper.loadByUnionPaySn(billTmp.getUnionPaySn());
        if(bill==null){
            throw new BasicException("数据库是不是没有保存好！");
        }
        Integer payChannel = bill.getPayChannel();
        switch (payChannel.intValue()) {// 0 支付宝 1 微信  2 银联
            case 0:{//支付宝
                AlipayInfo info = alipayInfoMapper.loadByShopId(bill.getShop_id());
                return AlipayUtils.pay(info,bill);
            }
            case 1:{//微信

            }
            case 2:{//银联

            }
            default: break;
        }
        return null;
    }

    @Override
    public PayBill loadByUnionPaySn(String unionPaySn) {
        return payBillMapper.loadByUnionPaySn(unionPaySn);
    }
}
