package cn.zxworkspace.pay.service;


import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.pay.domain.PayAccount;

public interface IPayAccountService extends IBaseService<PayAccount> {
}
