package cn.zxworkspace.pay.service;


import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.pay.domain.PayFlow;

public interface IPayFlowService extends IBaseService<PayFlow> {
}
