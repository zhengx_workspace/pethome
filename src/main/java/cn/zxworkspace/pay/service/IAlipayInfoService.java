package cn.zxworkspace.pay.service;


import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.pay.domain.AlipayInfo;

public interface IAlipayInfoService extends IBaseService<AlipayInfo> {
    AlipayInfo getByShopId(Long shop_id);
}
