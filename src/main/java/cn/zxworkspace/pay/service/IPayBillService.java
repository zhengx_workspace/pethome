package cn.zxworkspace.pay.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.pay.domain.PayBill;

public interface IPayBillService extends IBaseService<PayBill> {
    /**
     * 统一支付接口
     * @param bill 支付单
     * @return
     */
    String pay(PayBill bill);

    PayBill loadByUnionPaySn(String unionPaySn);
}
