package cn.zxworkspace.pay.controller;


import cn.zxworkspace.order.domain.AdoptOrder;
import cn.zxworkspace.order.service.IAdoptOrderService;
import cn.zxworkspace.pay.constants.PayConstants;
import cn.zxworkspace.pay.domain.AlipayInfo;
import cn.zxworkspace.pay.domain.PayBill;
import cn.zxworkspace.pay.domain.PayFlow;
import cn.zxworkspace.pay.service.IAlipayInfoService;
import cn.zxworkspace.pay.service.IPayBillService;
import cn.zxworkspace.pay.service.IPayFlowService;
import cn.zxworkspace.pay.utils.AlipayConfig;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@RestController
public class AlipayController {

    @Autowired
    private IPayBillService payBillService;

    @Autowired
    private IAlipayInfoService alipayInfoService;
    @Autowired
    private IAdoptOrderService adoptOrderService;
    @Autowired
    private IPayFlowService payFlowService;

    /**
     * 支付宝支付成功之后的异步通知接口
     */
    @PostMapping("/notify")
    public void notify(HttpServletRequest request) throws AlipayApiException {
        //获取支付宝POST过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map<String,String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        //得到统一支付标识
        String unionPaySn = params.get("out_trade_no");
        //根据支付表示获取订单表
        PayBill payBill = payBillService.loadByUnionPaySn(unionPaySn);
        AlipayInfo info = alipayInfoService.getByShopId(payBill.getShop_id());

        //确认身份
        boolean signVerified = AlipaySignature.rsaCheckV1(params,
                info.getAlipay_public_key(),
                AlipayConfig.charset,
                AlipayConfig.sign_type); //调用SDK验证签名

        if(signVerified) {//验证成功
            //商户订单号
            String out_trade_no = unionPaySn;
            //支付宝交易号
            String trade_no = request.getParameter("trade_no");
            //交易状态
            String trade_status = request.getParameter("trade_status");
            if(trade_status.equals("TRADE_FINISHED")){
                //......
            }else if (trade_status.equals("TRADE_SUCCESS")){
                // 调整统一支付单的状态
                payBill.setState(1);
                payBill.setUpdateTime(new Date());
                payBillService.update(payBill);
                // 记录支付流水  @TODO 自己玩
                PayFlow flow = new PayFlow();
                BeanUtils.copyProperties(payBill, flow);
                flow.setType(payBill.getPayChannel());
                flow.setBusinessName(payBill.getBusinessType());
                flow.setBusinessType(payBill.getBusinessType());
                flow.setBusinessKey(payBill.getBusinessKey());
                flow.setDigest(payBill.getDigest());
                payFlowService.add(flow);
                // 通过businessType + businessKey 修改某一个订单状态
                String businessType = payBill.getBusinessType();
                if(PayConstants.LINGYANG_ORDER.equals(businessType)){//领养订单
                    //修改订单状态
                    AdoptOrder order = adoptOrderService.queryById(payBill.getBusinessKey());
                    order.setState(1);
                    adoptOrderService.update(order);
                    // @TODO 删除支付超时定时任务

                }else if(PayConstants.FUWU_ORDER.equals(businessType)){//服务订单

                }

            }
        }else {//验证失败

        }

    }

}
