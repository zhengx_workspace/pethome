package cn.zxworkspace.pay.utils;

public class AlipayConfig {
    // 服务器异步通知页面路径
    public static String notify_url = "http://eaw6mh.natappfree.cc/notify";

    // 页面跳转同步通知页面路径
    public static String return_url = "http://localhost/success.html";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
}

