package cn.zxworkspace.pay.utils;

import cn.zxworkspace.pay.domain.AlipayInfo;
import cn.zxworkspace.pay.domain.PayBill;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;

/**
 * 支付宝支付工具类
 *    1 发起支付请求
 */
public class AlipayUtils {

    /**
     *
     * @param info 给哪个商家支付
     * @param bill  支付单
     * @return 支付请求数据包
     */
    public static String pay(AlipayInfo info, PayBill bill){
        try {
            //获得初始化的AlipayClient
            AlipayClient alipayClient = new DefaultAlipayClient(
                    AlipayConfig.gatewayUrl,
                    info.getAppid(),
                    info.getMerchant_private_key(),
                    "json",
                    AlipayConfig.charset, info.getAlipay_public_key(),
                    AlipayConfig.sign_type);

            //设置请求参数
            AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
            alipayRequest.setReturnUrl(AlipayConfig.return_url);
            alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

            //商户订单号，商户网站订单系统中唯一订单号，必填  //就是统一支付单单号
            String out_trade_no = bill.getUnionPaySn();
            //付款金额，必填
            String total_amount = bill.getMoney().toString();
            //订单名称，必填
            String subject = bill.getDigest();
            //商品描述，可空
            String body = bill.getDigest();

            alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                    + "\"total_amount\":\""+ total_amount +"\","
                    + "\"subject\":\""+ subject +"\","
                    + "\"body\":\""+ body +"\","
                    + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

            //请求
            String result = alipayClient.pageExecute(alipayRequest).getBody();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}