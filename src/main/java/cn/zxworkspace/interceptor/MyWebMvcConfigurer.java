package cn.zxworkspace.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration  // 申明当前类是Spring的配置类，=== ApplicatonContext.xml
public class MyWebMvcConfigurer implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor;


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**")//拦截所有请求
                .excludePathPatterns(  //放行资源
                        "/login/**",
                        "/user/phone/register",
                        "/shop/settlement",
                        "/fastdfs/**",
                        "/notify", //支付宝支付成功之后的异步回调地址
                        "/verifycode/**"
                        );

    }
}