package cn.zxworkspace.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

/**
 * 2.后端登录拦截器
 */
@Component  //交给spring容器管理
public class LoginInterceptor implements HandlerInterceptor {
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        String token = request.getHeader("token");
        //1.校验请求头中token是否有
        if(!StringUtils.isEmpty(token)){
            Object loginTmp = redisTemplate.opsForValue().get(token);
            //2.根据token获取redis中的登录信息
            if (loginTmp != null) {
                //放行,刷新过期时间
                redisTemplate.opsForValue().set(token,loginTmp,30, TimeUnit.MINUTES);
                return true;
            }
        }
        //报错
        response.setContentType("application/json;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        PrintWriter writer = response.getWriter();
        writer.write("{\"success\":false,\"message\":\"noLogin\"}");
        writer.flush();
        writer.close();
        return false;// 阻止放行

    }
}