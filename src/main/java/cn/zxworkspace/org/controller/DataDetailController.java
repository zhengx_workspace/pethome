package cn.zxworkspace.org.controller;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.org.domain.DataDetail;

import cn.zxworkspace.org.query.DataDetailQuery;
import cn.zxworkspace.org.service.IDataDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/datadetail")
public class DataDetailController {
    @Autowired
    private IDataDetailService datadetailService;
    /**
     * 新增或修改
     * @param datadetail
     * @return
     */
    @PostMapping("/save")
    public AjaxResult addOrUpdate(@RequestBody DataDetail datadetail) {
        try {
            if (datadetail.getId() != null) {
                datadetailService.update(datadetail);
            } else {
                datadetailService.add(datadetail);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败！" + e.getMessage());
        }
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable("id") Long id){
        try {
            datadetailService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败！" + e.getMessage());
        }
    }
    @PatchMapping
    public AjaxResult removeList(@RequestBody List<Long> ids){
        try {
            datadetailService.deleteList(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败！" + e.getMessage());
        }
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public DataDetail findById(@PathVariable("id") Long id){
        return datadetailService.queryById(id);
    }

    /**
     * 查询所有
     * @return
     */
    @GetMapping("/list")
    public List<DataDetail> findAll(){
        return datadetailService.queryAll();
    }

    /**\
     * 分页查询、高级查询
     * @param query
     * @return
     */
    @PostMapping
    public PageList<DataDetail> list(@RequestBody DataDetailQuery query){
        return datadetailService.queryPage(query);
    }
}
