package cn.zxworkspace.org.controller;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.org.domain.Employee;
import cn.zxworkspace.org.domain.Shop;
import cn.zxworkspace.org.query.ShopQuery;
import cn.zxworkspace.org.service.IShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shop")
public class ShopController {
    @Autowired
    private IShopService shopService;

    @PostMapping("/settlement")
    public AjaxResult settlement(@RequestBody Shop shop) {
        try {
            shopService.settlement(shop);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败！" + e.getMessage());
        }
    }



    /**
     * 新增或修改
     * @param shop
     * @return
     */
    @PostMapping("/save")
    public AjaxResult addOrUpdate(@RequestBody Shop shop) {
        try {
            if (shop.getId() != null) {
                shopService.update(shop);
            } else {
                shopService.add(shop);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败！" + e.getMessage());
        }
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable("id") Long id){
        try {
            shopService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败！" + e.getMessage());
        }
    }
    @PatchMapping
    public AjaxResult removeList(@RequestBody List<Long> ids){
        try {
            shopService.deleteList(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败！" + e.getMessage());
        }
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Shop findById(@PathVariable("id") Long id){
        return shopService.queryById(id);
    }

    /**
     * 查询所有
     * @return
     */
    @GetMapping("/list")
    public List<Shop> findAll(){
        return shopService.queryAll();
    }

    /**\
     * 分页查询、高级查询
     * @param query
     * @return
     */
    @PostMapping
    public PageList<Shop> list(@RequestBody ShopQuery query){
        return shopService.queryPage(query);
    }
}
