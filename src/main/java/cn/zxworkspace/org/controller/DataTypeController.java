package cn.zxworkspace.org.controller;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.org.domain.DataType;
import cn.zxworkspace.org.query.DataTypeQuery;
import cn.zxworkspace.org.service.IDataTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/datatype")
public class DataTypeController {
    @Autowired
    private IDataTypeService datatypeService;
    /**
     * 新增或修改
     * @param datatype
     * @return
     */
    @PostMapping("/save")
    public AjaxResult addOrUpdate(@RequestBody DataType datatype) {
        try {
            if (datatype.getId() != null) {
                datatypeService.update(datatype);
            } else {
                datatypeService.add(datatype);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败！" + e.getMessage());
        }
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable("id") Long id){
        try {
            datatypeService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败！" + e.getMessage());
        }
    }
    @PatchMapping
    public AjaxResult removeList(@RequestBody List<Long> ids){
        try {
            datatypeService.deleteList(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败！" + e.getMessage());
        }
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public DataType findById(@PathVariable("id") Long id){
        return datatypeService.queryById(id);
    }

    /**
     * 查询所有
     * @return
     */
    @GetMapping("/list")
    public List<DataType> findAll(){
        return datatypeService.queryAll();
    }

    /**\
     * 分页查询、高级查询
     * @param query
     * @return
     */
    @PostMapping
    public PageList<DataType> list(@RequestBody DataTypeQuery query){
        return datatypeService.queryPage(query);
    }
}
