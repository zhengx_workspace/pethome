package cn.zxworkspace.org.controller;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.org.domain.Department;
import cn.zxworkspace.org.query.DepartmentQuery;
import cn.zxworkspace.org.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dept")
public class DepartmentController {
    @Autowired
    private IDepartmentService departmentService;

    @GetMapping("/tree")
    public List<Department> findParent(){
        return departmentService.findParent();
    }
    /**
     * 新增或修改
     * @param department
     * @return
     */
    @PostMapping("/save")
    public AjaxResult addOrUpdate(@RequestBody Department department) {
        try {
            if (department.getId() != null) {
                departmentService.update(department);
            } else {
                departmentService.add(department);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败！" + e.getMessage());
        }
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable("id") Long id){
        try {
            departmentService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败！" + e.getMessage());
        }
    }
    @PatchMapping
    public AjaxResult removeList(@RequestBody List<Long> ids){
        try {
            departmentService.deleteList(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败！" + e.getMessage());
        }
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Department findById(@PathVariable("id") Long id){
        return departmentService.queryById(id);
    }

    /**
     * 查询所有
     * @return
     */
    @GetMapping("/list")
    public List<Department> findAll(){
        return departmentService.queryAll();
    }

    /**\
     * 分页查询、高级查询
     * @param query
     * @return
     */
    @PostMapping
    public PageList<Department> list(@RequestBody DepartmentQuery query){
        return departmentService.queryPage(query);
    }
}
