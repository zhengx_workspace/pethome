package cn.zxworkspace.org.controller;

import cn.zxworkspace.basic.util.AjaxResult;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.org.domain.Department;
import cn.zxworkspace.org.domain.Employee;

import cn.zxworkspace.org.query.EmployeeQuery;
import cn.zxworkspace.org.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private IEmployeeService employeeService;
    /**
     * 新增或修改
     * @param employee
     * @return
     */
    @PostMapping("/save")
    public AjaxResult addOrUpdate(@RequestBody Employee employee) {
        try {
            if (employee.getId() != null) {
                employeeService.update(employee);
            } else {
                employeeService.add(employee);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存失败！" + e.getMessage());
        }
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable("id") Long id){
        try {
            employeeService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败！" + e.getMessage());
        }
    }
    @PatchMapping
    public AjaxResult removeList(@RequestBody List<Long> ids){
        try {
            employeeService.deleteList(ids);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除失败！" + e.getMessage());
        }
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Employee findById(@PathVariable("id") Long id){
        return employeeService.queryById(id);
    }

    /**
     * 查询所有
     * @return
     */
    @GetMapping("/list")
    public List<Employee> findAll(){
        return employeeService.queryAll();
    }

    /**\
     * 分页查询、高级查询
     * @param query
     * @return
     */
    @PostMapping
    public PageList<Employee> list(@RequestBody EmployeeQuery query){
        return employeeService.queryPage(query);
    }
}
