package cn.zxworkspace.org.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

@Data
public class DataType extends BaseDomain {
    private String sn;
    private String name;
}
