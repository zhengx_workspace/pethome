package cn.zxworkspace.org.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

@Data
public class DataDetail extends BaseDomain {
    private String name;
    private DataType types;
    private Long types_id;
}
