package cn.zxworkspace.org.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

@Data
public class Employee extends BaseDomain {
    //xingming
    private String username;
    private String email;
    private String phone;
    private String salt;
    private String password;
    private String comfirmPassword;
    private Integer age;
    private Integer state = 0;
    private Department parent;
    private Long department_id;
    private Long logininfo_id;
    private Shop shops;
    private Long shop_id;

}
