package cn.zxworkspace.org.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

import java.util.Date;

@Data
public class Shop  extends BaseDomain {
    private String name;
    private String tel;
    private Date registerTime =new Date();
    private Integer state = 0;
    private String address;
    private String logo;
    private Employee admin;
    private Long admin_id;

}
