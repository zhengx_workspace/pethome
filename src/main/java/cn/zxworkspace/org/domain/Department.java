package cn.zxworkspace.org.domain;

import cn.zxworkspace.basic.domain.BaseDomain;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class Department extends BaseDomain {

    /*部门编号*/
    private String sn;
    /*部门名称*/
    private String name;
    /*暂时不用*/
    private String dirPath;
    /*部门状态 0 正常 ，-1 停用*/
    private Integer state;
    /*部门经理 和员工关联*/
    private Employee manager;
    private Department parent;
    private List<Department> children = new ArrayList<>();
}
