package cn.zxworkspace.org.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.org.domain.DataType;
import cn.zxworkspace.org.domain.Shop;

public interface DataTypeMapper extends BaseMapper<DataType> {

}
