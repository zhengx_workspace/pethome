package cn.zxworkspace.org.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.org.domain.Shop;
import cn.zxworkspace.org.query.ShopQuery;

import java.util.List;

public interface ShopMapper extends BaseMapper<Shop> {

}
