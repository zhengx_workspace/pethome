package cn.zxworkspace.org.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.org.domain.Employee;
import cn.zxworkspace.org.query.EmployeeQuery;


import java.util.List;

public interface EmployeeMapper extends BaseMapper<Employee> {

    Employee loadByname(String username);

    Employee loadByLogininfoId(Long id);
}
