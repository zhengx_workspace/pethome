package cn.zxworkspace.org.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.org.domain.DataDetail;
import cn.zxworkspace.org.domain.DataType;

public interface DataDetailMapper extends BaseMapper<DataDetail> {

}
