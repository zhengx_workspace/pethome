package cn.zxworkspace.org.mapper;

import cn.zxworkspace.basic.mapper.BaseMapper;
import cn.zxworkspace.org.domain.Department;
import cn.zxworkspace.org.query.DepartmentQuery;

import java.util.List;

public interface DepartmentMapper extends BaseMapper<Department> {

    List<Department> findTree();
}
