package cn.zxworkspace.org.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.org.domain.Shop;
import cn.zxworkspace.org.query.ShopQuery;

import java.util.List;

public interface IShopService extends IBaseService<Shop> {

    void settlement(Shop shop);
}
