package cn.zxworkspace.org.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.org.domain.Department;
import cn.zxworkspace.org.query.DepartmentQuery;

import java.util.List;

public interface IDepartmentService extends IBaseService<Department> {
    List<Department> findParent();

}
