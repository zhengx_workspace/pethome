package cn.zxworkspace.org.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.org.domain.Employee;
import cn.zxworkspace.org.query.EmployeeQuery;

import java.util.List;

public interface IEmployeeService extends IBaseService<Employee> {


}
