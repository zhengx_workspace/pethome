package cn.zxworkspace.org.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.org.domain.Department;
import cn.zxworkspace.org.mapper.DepartmentMapper;
import cn.zxworkspace.org.query.DepartmentQuery;
import cn.zxworkspace.org.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class DepartmentServiceImpl extends BaseServiceImpl<Department> implements IDepartmentService {
    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public List<Department> findParent() {
        return departmentMapper.findTree();
    }

    @Override
    @Transactional
    public void add(Department department) {
        departmentMapper.save(department);
        String dirPath = "";
        //如果parent——id不为空，说明自己是一级部门。只需要拼自己的dirpath
        if(department.getParent()==null){
            dirPath = "/"+department.getId();
        }else {
            //二级部门，则需要通过parent——id来获取上级部门的dirpath。然后拼接自己的dirpth
            dirPath = departmentMapper.loadById(department.getParent().getId()).getDirPath()+"/"+department.getId();
            System.out.println(dirPath);
        }
        department.setDirPath(dirPath);
        departmentMapper.update(department);
    }

    @Override
    @Transactional
    public void update(Department department) {
        String dirPath = "";
        //如果parent——id不为空，说明自己是一级部门。只需要拼自己的dirpath
        if(department.getParent()==null){
            dirPath = "/"+department.getId();
        }else {
            //二级部门，则需要通过parent——id来获取上级部门的dirpath。然后拼接自己的dirpth
            dirPath = departmentMapper.loadById(department.getParent().getId()).getDirPath()+"/"+department.getId();
            System.out.println(dirPath);
        }
        department.setDirPath(dirPath);
        departmentMapper.update(department);
    }


}
