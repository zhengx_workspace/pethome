package cn.zxworkspace.org.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.basic.util.MD5Utils;
import cn.zxworkspace.basic.util.PageList;
import cn.zxworkspace.basic.util.StrUtils;
import cn.zxworkspace.customer.domain.Logininfo;
import cn.zxworkspace.customer.mapper.LogininfoMapper;
import cn.zxworkspace.org.domain.Employee;

import cn.zxworkspace.org.mapper.EmployeeMapper;
import cn.zxworkspace.org.query.EmployeeQuery;
import cn.zxworkspace.org.service.IEmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class EmployeeServiceImpl extends BaseServiceImpl<Employee> implements IEmployeeService {
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private LogininfoMapper logininfoMapper;

    @Override
    public void add(Employee employee) {
        //初始化Employee的必要字段
        initEmployee(employee);
        //拷贝一个logininfo
        Logininfo logininfo = employee2Logininfo(employee);
        logininfoMapper.save(logininfo);
        employee.setLogininfo_id(logininfo.getId());
        employeeMapper.save(employee);
    }

    private Logininfo employee2Logininfo(Employee employee) {
        Logininfo logininfo = new Logininfo();
        BeanUtils.copyProperties(employee, logininfo);
        logininfo.setType(0);//后台员工
        return logininfo;
    }

    private void initEmployee(Employee employee) {
        String salt = StrUtils.getComplexRandomString(32);
        //使用盐值加密
        String md5Pwd = MD5Utils.encrypByMd5(employee.getPassword() + salt);
        employee.setSalt(salt);
        employee.setPassword(md5Pwd);
    }

}
