package cn.zxworkspace.org.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.org.domain.DataDetail;
import cn.zxworkspace.org.domain.DataType;
import cn.zxworkspace.org.service.IDataDetailService;
import cn.zxworkspace.org.service.IDataTypeService;
import org.springframework.stereotype.Service;

@Service
public class DataDetailServiceImpl extends BaseServiceImpl<DataDetail> implements IDataDetailService {


}
