package cn.zxworkspace.org.service.impl;

import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.org.domain.DataType;
import cn.zxworkspace.org.domain.Shop;
import cn.zxworkspace.org.mapper.ShopMapper;
import cn.zxworkspace.org.service.IDataTypeService;
import cn.zxworkspace.org.service.IShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataTypeServiceImpl extends BaseServiceImpl<DataType> implements IDataTypeService {


}
