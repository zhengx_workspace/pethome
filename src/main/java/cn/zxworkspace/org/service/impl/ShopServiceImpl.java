package cn.zxworkspace.org.service.impl;

import cn.zxworkspace.basic.exception.BasicException;
import cn.zxworkspace.basic.service.impl.BaseServiceImpl;
import cn.zxworkspace.org.domain.Employee;
import cn.zxworkspace.org.domain.Shop;
import cn.zxworkspace.org.mapper.EmployeeMapper;
import cn.zxworkspace.org.mapper.ShopMapper;
import cn.zxworkspace.org.service.IEmployeeService;
import cn.zxworkspace.org.service.IShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShopServiceImpl extends BaseServiceImpl<Shop> implements IShopService {
    @Autowired
    private ShopMapper shopMapper;
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private IEmployeeService employeeService;

    @Override
    public void settlement(Shop shop) {
        Employee admin = shop.getAdmin();
        if(!admin.getPassword().equals(admin.getComfirmPassword())){
            throw new BasicException("两次密码输入不一致");
        }
        //根据管理员的名字查询是否有这个员工
        Employee employee = employeeMapper.loadByname(admin.getUsername());
        //如果员工不为空，则说明有这个账户
        if(employee!=null){
            throw new BasicException("账户已存在，请直接登录");
        }
        //先保存这个员工，返回自增id。
        employeeService.add(admin);
        //employeeMapper.save(admin);
        //设置入驻店铺的管理员
        shop.setAdmin(admin);
        //再保存店铺
        shopMapper.save(shop);
        //设置员工所在的店铺id
        admin.setShops(shop);
        employeeMapper.update(admin);
//        shop.setAdmin_id(admin.getId());
        //修改店铺
//        shopMapper.update(shop);
    }

}
