package cn.zxworkspace.org.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.org.domain.DataDetail;
import cn.zxworkspace.org.domain.DataType;

public interface IDataDetailService extends IBaseService<DataDetail> {

}
