package cn.zxworkspace.org.service;

import cn.zxworkspace.basic.service.IBaseService;
import cn.zxworkspace.org.domain.DataType;
import cn.zxworkspace.org.domain.Shop;

public interface IDataTypeService extends IBaseService<DataType> {

}
